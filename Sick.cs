﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sick : MonoBehaviour
{
    bool isSick = true; 
    GameObject citizenMasterGroup; 
    GameObject citizenCurrent; 

    private void Start() 
    {
        transform.tag = "sick";
        StartCoroutine("GetBetter"); 
        citizenCurrent = this.gameObject;  
        citizenMasterGroup = GameObject.FindWithTag("Flu"); 
    }

    private void Update() 
    {
        Collider[] hits = Physics.OverlapSphere(transform.position, 1);

        foreach (Collider hit in hits)
        {
            if (hit.tag == "Citizen")
            {
                MakeSick(); 
            }
        }

        if (isSick == false)
        {
            Better (); 
        }
    }
    
    IEnumerator GetBetter() //makes citizens healthy over time
    {
        yield return new WaitForSeconds(15); //change this number for longer or shorter get better periods of time

        isSick = false;
    }

    void MakeSick () //raycast that makes healthy citizens that come into contact with a sick person sick
    {   
        Collider[] hits = Physics.OverlapSphere(transform.position, 1);

        foreach (Collider hit in hits)
        {
            if (hit.tag == "Citizen")
            {
                hit.tag = "sick"; 
                hit.GetComponent<Sick>().enabled = true; 
                citizenMasterGroup.GetComponent<FluScript>().MoreSick(); //refrences int counter of sick citizens in main script
            }
        }
        
    }

    void Better () //makes the citizens better
    {
        StopCoroutine("GetBetter"); 
        citizenMasterGroup.GetComponent<FluScript>().LessSick(); //refrences int counter of sick citizens in main script
        citizenCurrent.tag = "Citizen";
        citizenCurrent.GetComponent<Sick>().enabled = false; //disables this script
    }
}
