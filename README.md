# Illness System

A coding system in C# that controls the illness (like a flu) in a mini city in Unity. Citizens get sick, can spread the illness amongst each other, and get better over time. 

This project was for a university group project where we built a small self-sustaining city. The code in this specifically gets one person sick after a certain amount of time and spreads it around the city.

To use this you'll need a group of prefabs which will serve as the citizens in a city. The main group collection/empty that have all these prefabs will need to have the FluScript attached to it. This group will need a tag like "GroupOfCitizens". And the citizen prefab will need to have the Sick script attached (which should be disabled on the prefab). All citizens will need to be tagged one thing like "Citizen" for this code to create the list of citizens to pick a random citizen to get sick. This would need to be changed in the code itself if they have a specific name you chose. Then an extra tag called "sick" will need to be made to track which citizens are sick and to get others sick, while avoiding getting sick again themselves.There is a UI counter for the script to show players how many people are sick. When you make the text for this you'll need to assign it in the main script in the inspector so the code can work. 
